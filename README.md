# Dotfiles
Configuration files for Linux programs
managed with gnu stow

<br>

## 📚 Dependencies
| Package                                                       | Description            |
| ------------------------------------------------------------- | ---------------------- |
| [bash](https://www.gnu.org/software/bash/)                    | run the script         |
| [stow](https://www.gnu.org/software/stow/)                    | create the symlinks    |
| [find](https://www.gnu.org/software/findutils/) (optional)    | remove broken symlinks |

[See the available configurations](src/)

<br>

## 🚀 Installation
```bash
# Clone this repository in ~/.dotfiles
$ git clone git@gitlab.com:yowls/dotfiles.git ~/.dotfiles

# Modify the settings.sh and then run
$ ./rice.sh install
```

<br>

## 🔥 Uninstall
```bash
# To delete all symlinks created run:
$ ./rice purge
```

<br>

## ⚙️ Usage
```bash
# Everytime change a settings run:
$ ./rice.sh sync
```
