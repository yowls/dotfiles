#     __ _     _           _          _ _
#    / _(_)___| |__    ___| |__   ___| | |
#   | |_| / __| '_ \  / __| '_ \ / _ \ | |
#   |  _| \__ \ | | | \__ \ | | |  __/ | |
#   |_| |_|___/_| |_| |___/_| |_|\___|_|_|

# Commands to run in interactive sessions
if status is-interactive
	# IMPORT SOURCES
	#-------------------
	#source $HOME/.config/fish/functions.fish


	# EXPORT VARIABLES
	#-------------------
	# export ..


	# OPTIONS
	#-------------------
	# hide welcome message
	set fish_greeting

	# Fallback Theme
	set fish_color_normal brcyan
	set fish_color_autosuggestion '#7d7d7d'
	set fish_color_command brcyan
	set fish_color_error '#ff6c6b'
	set fish_color_param brcyan


	# PLUGINS
	#-------------------
	# source /usr/share/autojump/autojump.fish


	# DIFFS IN SHELL
	# ------------------
	# Colorize Man Pages
	set -x LESS_TERMCAP_mb (printf "\033[01;31m")
	set -x LESS_TERMCAP_md (printf "\033[01;31m")
	set -x LESS_TERMCAP_me (printf "\033[0m")
	set -x LESS_TERMCAP_se (printf "\033[0m")
	set -x LESS_TERMCAP_so (printf "\033[01;44;33m")
	set -x LESS_TERMCAP_ue (printf "\033[0m")
	set -x LESS_TERMCAP_us (printf "\033[01;32m")


	# DIFFS IN PROFILE
	# ------------------
	[ -f $HOME/.config/fish/profile.fish ] && source $HOME/.config/fish/profile.fish


	## ALIASES
	# ------------------
	source $HOME/.config/shell_alias/shared
	[ -f $HOME/.config/shell_alias/profile ] && source "$HOME/.config/shell_alias/profile"
end
