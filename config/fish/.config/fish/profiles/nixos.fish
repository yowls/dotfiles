#!/usr/bin/env fish

# Gnome keyring
#if test -n "$DESKTOP_SESSION"
#	set -x (gnome-keyring-daemon --start | string split "=")
#end

# Plugins
#=> z.lua - autojump helper
/run/current-system/sw/bin/z.lua --init fish | source
# set -gx _ZL_CD cd

# Theme: Starship prompt
starship init fish | source
