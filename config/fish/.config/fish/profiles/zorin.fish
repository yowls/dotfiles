#!/usr/bin/env fish

# Gnome keyring
if test -n "$DESKTOP_SESSION"
	set -x (gnome-keyring-daemon --start | string split "=")
end

# Plugins
#=> Pazi - autojump helper
if command -v pazi >/dev/null
	pazi init fish | source
end

# Theme: Starship prompt
starship init fish | source
