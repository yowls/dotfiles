# Export variables
# TODO: set if unset
export VISUAL="/usr/bin/nvim"
export EDITOR="/usr/bin/nvim"

# Commands to run in interactive sessions
if status is-interactive
	# OPTIONS
	set fish_greeting

	# Colorize Man Pages
	set -x LESS_TERMCAP_mb (printf "\033[01;31m")
	set -x LESS_TERMCAP_md (printf "\033[01;31m")
	set -x LESS_TERMCAP_me (printf "\033[0m")
	set -x LESS_TERMCAP_se (printf "\033[0m")
	set -x LESS_TERMCAP_so (printf "\033[01;44;33m")
	set -x LESS_TERMCAP_ue (printf "\033[0m")
	set -x LESS_TERMCAP_us (printf "\033[01;32m")


	# THEME
	# fallback Theme
	set fish_color_normal brcyan
	set fish_color_autosuggestion '#7d7d7d'
	set fish_color_command brcyan
	set fish_color_error '#ff6c6b'
	set fish_color_param brcyan

	# Run starship prompt
	starship init fish | source


	# ALIAS
	alias grupdate="grub-mkconfig -o /boot/grub/grub.cfg"
	alias grup2date="grub2-mkconfig -o /boot/grub2/grub.cfg"
	alias gruconf="$EDITOR /etc/default/grub"
	alias p="ps -el | grep"
	alias rswap="swapoff -a && sudo swapon -a"
	alias lsbok="lsblk -o name,mountpoint,label,size,uuid"
	alias partid="blkid -s UUID -o value"
	alias ficonf="$EDITOR ~/.config/fish/config.fish"
	alias nvconf="$EDITOR ~/.config/nvim/init.vim"

	# Put Colors
	alias grep='grep --color=auto'
	alias egrep='egrep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias dd='dd status=progress'

	alias ls="ls -hN --color=auto --group-directories-first"
	alias la="ls -a --color=auto --group-directories-first"
	alias ll="ls -la --color=auto --group-directories-first"
	alias l="ls -l --color=auto --group-directories-first"
	alias vi="nvim"
	alias vim="nvim"
end
