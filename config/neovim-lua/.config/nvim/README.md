# NEOVIM CONFIG IN LUA


### Dependencies
+ [npm](https://www.npmjs.com/)
+ [yarn](https://yarnpkg.com/)
+ [ripgrep](https://github.com/BurntSushi/ripgrep)
+ [fd](https://github.com/sharkdp/fd)
+ gcc-c++
+ libstdc++

<br>

### Common LSP
require to install

+ npm i -g vscode-langservers-extracted
+ npm i -g bash-language-server

<br>

### Pluggins
list of pluggins

+ [packer](https://github.com/wbthomason/packer.nvim)
