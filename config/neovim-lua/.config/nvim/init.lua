--[[
.   _ __   ___  _____   _(_)_ __ ___
.  | '_ \ / _ \/ _ \ \ / / | '_ ` _ \
.  | | | |  __/ (_) \ V /| | | | | | |
.  |_| |_|\___|\___/ \_/ |_|_| |_| |_|
.     gitlab.com/yowls/dotfiles
--]]

---
-- IMPORT MODULES
---

require('global')
require('keymaps')
require('theme')
require('plugins')
require('lsp')
