---
-- NEOVIM GENERAL CONFIG
---

vim.opt = {
	-- Syntax
	foldmethod = "syntax",

	-- Search
	ignorecase	= true,
	smartcase	= true,
	hlsearch	= true,
	incsearch	= true,
	showmatch	= true,
	inccommand	= "split",

	-- Indentation
	-- expandtab	= false,
	autoindent		= true,
	smartindent		= true,
	-- smarttab	= true,
	tabstop			= 4,
	shiftwidth		= 4,
	softtabstop		= 0,
	shiftround		= true,

	-- Line
	number			= true,
	numberwidth		= 2,
	relativenumber	= true,
	wrap			= true,
	linebreak		= true,
	list			= true,
	listchars		= { tab = "⇒ ", space = "·", trail = "×" },

	-- Cursor
	cursorline		= true,
	cursorcolumn	= true,
	scrolloff		= 2,

	-- Colors
	termguicolors	= true,
	colorcolumn		= "80",

	-- Buffers
	hidden			= true,
	splitright		= true,
	splitbelow		= true,
	sidescrolloff	= 8,

	-- Swap file
	-- swapfile	= false,

	-- Menu
	-- completeopt	= "menu,menuone,noselect",
	completeopt		= "menu,preview",

	-- Various
	-- TODO: move undodir to ~/.cache/vim_undodir/
	mouse		= "a",
	clipboard	= "unnamedplus",
	backspace	= "indent,eol,start",
	undofile	= true,
	undodir		= vim.fn.stdpath('config') .. '/.undodir'
}

vim.cmd [[au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif"]]

-- File type
vim.cmd("autocmd FileType scss setl iskeyword+=@-@")
vim.cmd("autocmd FileType haskell setl expandtab")


-- vim: filetype=lua tabstop=4 shiftwidth=4 softtabstop=0 noexpandtab nowrap
