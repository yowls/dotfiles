local modes = {}

---
-- PURE KEYMAPS
---

-- Set leader
vim.g.mapleader = " "


-- Normal mode keymaps
-- TODO: add close buffer command
modes.n = {
	-- Cycle buffers
	--["<C-q>"] = ":bp<br>bd#<CR>",				-- close
	["<Tab>"] = ":bnext<CR>",		-- next
	["<S-Tab>"] = ":bprevious<CR>",	-- previous

	-- Focus window buffers
	["<A-j>"] = "<C-W><C-j>",
	["<A-k>"] = "<C-W><C-k>",
	["<A-h>"] = "<C-W><C-h>",
	["<A-l>"] = "<C-W><C-l>",

	-- Resize window
	["<A-S-k>"] = ":resize +5<CR>",
	["<A-S-j>"] = ":resize -5<CR>",
	["<A-S-h>"] = ":vertical resize +5<CR>",
	["<A-S-l>"] = ":vertical resize -5<CR>",

	-- Fast scroll
	["<C-j>"] = "10<C-e>",
	["<C-k>"] = "10<C-y>",

	-- Move line
	["J"] = ":m .+1<CR>==",
	["K"] = ":m .-2<CR>==",

	-- Others
	["A-w"] = ":set wrap!<CR>"	-- toggle wrap line
}


-- Visual mode keymaps
modes.v = {
	-- Move line
	["J"] = ":m '>+1<CR>gv=gv",
	["K"] = ":m '<-2<CR>gv=gv",
}


-- Set keymaps
local options = { noremap = true, silent = true }

for mode in pairs(modes) do
	for key,cmd in pairs(modes[mode]) do
		vim.api.nvim_set_keymap(mode, key, cmd, options)
	end
end
