---
-- Init some LSP clients
---

local settings = require('settings')


if settings.enable_lua_lsp then
	require('lsp.sumneko_lua')
end

if settings.enable_python_lsp then
	require('lspconfig').pyright.setup{}
end
