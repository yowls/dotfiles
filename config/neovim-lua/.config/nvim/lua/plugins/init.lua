---
-- NEOVIM PLUGINS
---
-- TODO: add bootstrapping code
-- TODO: re-format all code


vim.cmd [[packadd packer.nvim]]

-- add plugins keymaps
require('plugins.keymaps')

return require('packer').startup(function(use)
	-- SELF
	use { 'wbthomason/packer.nvim' }

	--==============================

	-- SYNTAX
	-- TODO: check https://github.com/yamatsum/nvim-cursorline
	-- TODO: check surround; easy replace: () -> {} keybind
	-- TODO: check more treesitter extensions

	use { 'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate',
		config = function()
			require('plugins.treesitter')
		end
	}

	use { 'ntpeters/vim-better-whitespace' }

	-- TODO: create config and set {|} + space -> to { | }
	use { 'windwp/nvim-autopairs',
		config = function()
			require('nvim-autopairs').setup {}
		end
	}

	use { 'p00f/nvim-ts-rainbow' }

	use { 'andymass/vim-matchup',
		event = 'VimEnter'
	}

	use { "lukas-reineke/indent-blankline.nvim",
		event = "BufRead",
		config = function()
			require('plugins.indent_blankline')
		end
	}

	use { 'numToStr/Comment.nvim',
		config = function()
			require('plugins.comment').setup {}
		end
	}

	use { "folke/todo-comments.nvim",
		requires = "nvim-lua/plenary.nvim",
		config = function()
			require('todo-comments').setup {}
		end
	}

	use { 'lewis6991/gitsigns.nvim',
		requires = { 'nvim-lua/plenary.nvim' },
		config = function()
			require('gitsigns').setup {}
		end
	}

	--==============================

	-- SEARCH AND MOVEMENT
	-- TODO: add multi cursors
	-- TODO: add extensions to telescope

	use { 'nvim-telescope/telescope.nvim',
		requires = { 'nvim-lua/plenary.nvim' },
		config = function()
			require('plugins.telescope')
		end
	}

	use { 'phaazon/hop.nvim', branch = 'v1',
		config = function()
			require'hop'.setup { keys = 'etovxqpdygfblzhckisuran' }
		end
	}

	--==============================

	-- STATUSLINE AND BUFFERLINE
	use { 'nvim-lualine/lualine.nvim',
		requires = {'kyazdani42/nvim-web-devicons', opt = true},
		config = function()
			require('plugins.lualine')
		end
	}

	use { 'akinsho/bufferline.nvim',
		requires = { 'kyazdani42/nvim-web-devicons' },
		config = function ()
			require("bufferline").setup{}
		end
	}

	--==============================

	-- EXTRA PANELS
	-- TODO: add other panel to the right (tagbar?)

	use { 'kyazdani42/nvim-tree.lua',
		requires = { 'kyazdani42/nvim-web-devicons' },
		config = function()
			require('plugins.nvim_tree')
		end
	}

	use { "akinsho/toggleterm.nvim",
		config = function ()
			-- require('plugins.toggleterm')
			require("toggleterm").setup{}
		end
	}

	--==============================

	-- LANGUAGES
	-- TODO: { prettier or formatter ?? }

	use { 'norcalli/nvim-colorizer.lua',
		event = "BufRead",
		config = function()
			require('plugins.colorizer')
		end
	}

	use { 'iamcco/markdown-preview.nvim',
		run = 'cd app && yarn install'
	}

	use { 'cespare/vim-toml', branch = 'main' }

	use { 'LnL7/vim-nix' }

	use { 'leafo/moonscript-vim' }

	--==============================

	-- LSP
	-- TODO: check https://github.com/rockerBOO/awesome-neovim#lsp
	-- TODO: add dap or sniprun
	-- TODO: add snippets
	use { "neovim/nvim-lspconfig" }

	use { 'hrsh7th/nvim-cmp',
		requires = { 'hrsh7th/cmp-nvim-lsp' },
		config = function ()
			require('plugins.nvim_cmp')
		end
	}

	use { "folke/trouble.nvim",
		requires = "kyazdani42/nvim-web-devicons",
		config = function()
			require("trouble").setup {}
		end
	}

	--==============================

	-- THEMES
	use { 'folke/tokyonight.nvim' }

	--==============================

	-- EXTRAS:
	-- https://github.com/karb94/neoscroll.nvim
	-- https://github.com/andweeb/presence.nvim

	--==============================

	-- LIBRARIES:
	-- use { 'nvim-lua/plenary.nvim' }
	-- use_rocks 'penlight'
end)
