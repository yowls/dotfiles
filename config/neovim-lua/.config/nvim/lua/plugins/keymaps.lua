local plugins = {}

---
-- PLUGINS KEYMAPS
---

-- TODO: set plugin>mode>keymap instead of mode>plugin>keymap


-- Normal mode keymaps
plugins.n = {
	-- Hop (motions)
	["<leader>w"] = ":HopWord<CR>",

	-- Nvim-tree
	["<C-m>"] = ":NvimTreeToggle<CR>",
	["<leader>r"] = ":NvimTreeRefresh<CR>",
	["<leader>f"] = ":NvimTreeFindFile<CR>",

	-- Telescope
	["<C-Space>"] = "<cmd>Telescope find_files<cr>",
	["<leader>s"] = "<cmd>Telescope live_grep<cr>",
	["<leader>b"] = "<cmd>Telescope buffers<cr>",
	["<leader>h"] = "<cmd>Telescope help_tags<cr>"

	-- Lsp-config
}

-- Set keymaps
local options = { noremap = true, silent = true }

for mode in pairs(plugins) do
	for key,cmd in pairs(plugins[mode]) do
		vim.api.nvim_set_keymap(mode, key, cmd, options)
	end
end
