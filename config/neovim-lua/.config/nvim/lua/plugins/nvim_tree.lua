---
-- NVIM TREE CONFIG
---

-- Open when enter
-- vim.cmd [[autocmd VimEnter * NvimTreeFindFile]]

-- Old vim options
-- vim.g.nvim_tree_gitignore		= 1 -- [deprecated]
vim.g.nvim_tree_git_hl			= 1
vim.g.nvim_tree_indent_markers	= 1
vim.g.nvim_tree_add_trailing	= 1
vim.g.nvim_tree_highlight_opened_files = 1

-- Lua format options
require('nvim-tree').setup {
	disable_netrw       = true,
	hijack_netrw        = true,
	open_on_setup       = true,
	ignore_ft_on_setup  = {},
	auto_close          = true,
	open_on_tab         = false,
	hijack_cursor       = false,
	update_cwd          = false,

	update_to_buf_dir   = {
		enable = true,
		auto_open = true,
	},

	diagnostics = {
		enable = false,
		icons = {
			hint = "",
			info = "",
			warning = "",
			error = "",
		}
	},

	update_focused_file = {
		enable      = false,
		update_cwd  = false,
		ignore_list = {}
	},

	system_open = {
		cmd  = nil,
		args = {}
	},

	filters = {
		dotfiles = false,
		custom = {
			".git",
			"node_modules",
			"__pycache__", ".pyc",
			".hi", ".o"
		}
	},

	view = {
		width = 30,
		height = 30,
		hide_root_folder = false,
		side = 'left',
		auto_resize = true,
		mappings = {
			custom_only = false,
			list = {}
		}
	}
}
