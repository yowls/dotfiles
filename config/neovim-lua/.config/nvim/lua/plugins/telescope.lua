require('telescope').setup {
	defaults = {
		mappings = {
			i = {
			-- map actions.which_key to <C-h> (default: <C-/>)
			-- actions.which_key shows the mappings for your picker,
			-- e.g. git_{create, delete, ...}_branch for the git_branches picker
				["<C-h>"] = "which_key"
			}
		}
	},

	pickers = {
		-- Now the picker_config_key will be applied every time you call this
		-- builtin picker
	},

	extensions = {
		-- please take a look at the readme of the extension you want to configure
	}
}
