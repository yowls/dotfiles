require("toggleterm").setup{
	-- size can be a number or function which is passed the current terminal
	size = 20 | function(term)
		if term.direction == "horizontal" then
			return 15
		elseif term.direction == "vertical" then
			return vim.o.columns * 0.4
		end
	end,

	open_mapping = [[<c-\>]],
	hide_numbers = true,
	shade_filetypes = {},
	shade_terminals = true,
	-- shading_factor = '<number>',
	start_in_insert = true,
	insert_mappings = true,
	persist_size = true,
	direction = 'vertical' | 'horizontal' | 'window' | 'float',
	close_on_exit = true,
	shell = vim.o.shell,

	-- This field is only relevant if direction is set to 'float'
	float_opts = {
		-- The border key is *almost* the same as 'nvim_open_win'
		-- see :h nvim_open_win for details on borders however
		-- the 'curved' border is a custom border type
		-- not natively supported but implemented in this plugin.
		border = 'single' | 'double' | 'shadow' | 'curved',
		width = 120,
		height = 40,
		winblend = 3,
		highlights = {
			border = "Normal",
			background = "Normal",
		}
	}
}
