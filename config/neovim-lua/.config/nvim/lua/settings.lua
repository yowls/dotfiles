---
-- USER SETTINGS
---

local settings = {}

-- MODE
-- NOTE: not ready
-- set how much bloated you want
-- value: light | normal | heavy
settings.mode = "normal"


-- LSP Integration
-- NOTE: require aditional steps
-- NOTE: check: https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
-- TODO: add: vscode-langservers-extracted
settings.enable_lua_lsp		= true
settings.enable_python_lsp	= false


-- End
return settings
