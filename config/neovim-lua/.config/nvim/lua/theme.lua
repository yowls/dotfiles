-- Options
vim.g.tokyonight_style = "storm"
vim.g.tokyonight_italic_functions = true


-- Set the color scheme
vim.cmd[[colorscheme tokyonight]]
