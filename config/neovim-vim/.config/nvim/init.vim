" ██ ███    ██ ██ ████████ ███    ██ ██    ██ ██ ███    ███
" ██ ████   ██ ██    ██    ████   ██ ██    ██ ██ ████  ████
" ██ ██ ██  ██ ██    ██    ██ ██  ██ ██    ██ ██ ██ ████ ██
" ██ ██  ██ ██ ██    ██    ██  ██ ██  ██  ██  ██ ██  ██  ██
" ██ ██   ████ ██    ██ ██ ██   ████   ████   ██ ██      ██
" -_- -_-_ _-_- _--_ --_ -_-_ -_-_- -_-_-_ -_-_- _-_-_ -_-_

" INIT PLUGINS
call plug#begin('~/.config/nvim/plugged')
	" Syntax
	Plug 'Yggdroot/indentLine'
	Plug 'andymass/vim-matchup'
	Plug 'ntpeters/vim-better-whitespace'
	Plug 'jiangmiao/auto-pairs'
	Plug 'preservim/nerdcommenter'
	Plug 'airblade/vim-gitgutter'

	" Search and movement
	Plug 'ctrlpvim/ctrlp.vim'
	Plug 'easymotion/vim-easymotion'
	Plug 'mg979/vim-visual-multi', {'branch': 'master'}

	" Statusline and bufferline
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'

	" Extra Panels
	Plug 'preservim/nerdtree'
	Plug 'Xuyuanp/nerdtree-git-plugin'
	Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
	Plug 'preservim/tagbar'
	Plug 'nikvdp/neomux'

	" LSP
	Plug 'neoclide/coc.nvim', {'branch': 'release'}

	" Languages
	Plug 'LnL7/vim-nix'
	Plug 'khaveesh/vim-fish-syntax'
	Plug 'ap/vim-css-color'
	Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
	Plug 'cespare/vim-toml', { 'branch': 'main' }
	Plug 'leafo/moonscript-vim'

	" Theme
	Plug 'ryanoasis/vim-devicons'
	Plug 'drewtempelmeyer/palenight.vim'

call plug#end()


" BASICS
"##############
syntax on
set noexpandtab
set autoindent
set copyindent
set tabstop=4
set shiftwidth=4
set softtabstop=0

" File type options
filetype plugin indent on
autocmd FileType scss setl iskeyword+=@-@
autocmd FileType haskell setl expandtab

" Search
set ignorecase
set smartcase
set hlsearch
set incsearch
set showmatch

" Wildmenu
set wildmenu
set wildmode=longest,list,full
set wildignore+=*/tmp/*,*.so,*.swp,*.zip

" Various
set magic
set number relativenumber
set wrap linebreak
set encoding=utf-8
set clipboard=unnamedplus
set nocompatible
set mouse=a
set list
set hidden
set scrolloff=1

" Persist undo history
set undodir=~/.config/nvim/.undodir
set undofile

" Enter in the position where you left off
au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif


" VIM KEYBINDS
"##############
" set mapleader to spacebar
let mapleader = " "

" Resize window
nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> <Leader>- :exe "resize " . (winheight(0) * 2/3)<CR>

" Fast scroll
nnoremap <C-j> 10<C-e>
nnoremap <C-k> 10<C-y>

" VScode like move up and down commands
nnoremap J :m .+1<CR>==
nnoremap K :m .-2<CR>==
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Toggle wrap
map <C-k> :set wrap!<CR>

" Buffers
noremap <C-q> :bp<bar>bd#<Cr>
nnoremap <silent> <Tab> :bn<CR>
nnoremap <silent> <S-Tab> :bp<CR>


" THEME
"##############
if (has("termguicolors"))
	set termguicolors
endif
set background=dark
colorscheme palenight
let g:palenight_terminal_italics=1

" Cursor
"set cursorcolumn
set cursorline
hi CursorLine cterm=NONE ctermbg=239
" vertical column
set colorcolumn=100
highlight ColorColumn ctermbg=DarkGray guibg=DarkGray


" PLUGINS CONFIG
"##################
" Airline
" let g:airline_theme='minimalist'
let g:airline_theme='palenight'
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

" Better Whitespaces
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1

" Ctrlp
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_map = '<c-Space>'
nmap <C-b> :CtrlPBuffer<CR>

" Easy motion
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)
" s{char}{char} to move to {char}{char}
" nmap s <Plug>(easymotion-overwin-f2)
" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)
" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)
" Gif config
nmap s <Plug>(easymotion-s2)
nmap t <Plug>(easymotion-t2)

" Commenter
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1
vmap <C-c> <plug>NERDCommenterToggle
nmap <C-c> <plug>NERDCommenterToggle

" COC
inoremap <silent><expr> <S-TAB>
	\ pumvisible() ? "\<C-n>" :
	\ <SID>check_back_space() ? "\<TAB>" :
	\ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~# '\s'
endfunction
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Sidebar NERDTree
autocmd VimEnter * NERDTree | wincmd p
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let NERDTreeIgnore = ['\.pyc$', '__pycache__', '\.hi$', '\.o$']
let g:NERDTreeWinSize=25
map <C-m> :NERDTreeToggle<CR>
map <C-f> :NERDTreeFind<CR>

" Sidebar Tagbar
let g:Tlist_WinWidth=20
nmap <F10> :TagbarToggle<CR>

" GitGutter
" set GitGutterBufferEnable
" set GitGutterLineNrHighlightsEnable
" set updatetime=2000
highlight GitGutterAdd    guifg=#06d6a0 ctermfg=2
highlight GitGutterChange guifg=#ffd166 ctermfg=3
highlight GitGutterDelete guifg=#ef476f ctermfg=1
let g:gitgutter_max_signs = 500
let g:gitgutter_sign_added = '│'
let g:gitgutter_sign_modified = '~'
let g:gitgutter_sign_removed = '×'
let g:gitgutter_sign_removed_first_line = '^'
let g:gitgutter_sign_removed_above_and_below = '÷'
let g:gitgutter_sign_modified_removed = '!'
nmap ) <Plug>(GitGutterNextHunk)
nmap ( <Plug>(GitGutterPrevHunk)
" map <C-i> :GitGutterLineHighlightsToggle<CR>

" Markdown preview
" let g:mkdp_auto_start = 0
