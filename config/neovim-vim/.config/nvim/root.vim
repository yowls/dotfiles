" Minimial nvim config for root

" BASICS
syntax on
set noexpandtab
set autoindent
set copyindent
set tabstop=4
set shiftwidth=4
set softtabstop=0
" File type options
filetype plugin indent on
autocmd FileType scss setl iskeyword+=@-@
autocmd FileType haskell setl expandtab
" Search
set ignorecase
set smartcase
set hlsearch
set incsearch
set showmatch
" Wildmenu
set wildmenu
set wildmode=longest,list,full
set wildignore+=*/tmp/*,*.so,*.swp,*.zip
" Various
set magic
set number relativenumber
set wrap linebreak
set encoding=utf-8
set clipboard=unnamedplus
set nocompatible
set mouse=a
set list
set hidden
set scrolloff=1
" Persist undo history
set undodir=~/.config/nvim/undodir
set undofile
" Enter in the position where you left off
au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif


" KEYBINDS
" set mapleader to spacebar
let mapleader = " "

" Resize window
nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> <Leader>- :exe "resize " . (winheight(0) * 2/3)<CR>

" VScode like move up and down commands
nnoremap J :m .+1<CR>==
nnoremap K :m .-2<CR>==
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
" Toggle wrap
map <C-k> :set wrap!<CR>
" Buffers
noremap <C-q> :bd<Cr>
nnoremap <silent> <Tab> :bn<CR>
nnoremap <silent> <S-Tab> :bp<CR>


" Theme
set background=dark
" Cursor
"set cursorcolumn
set cursorline
hi CursorLine cterm=NONE ctermbg=239
" vertical column
set colorcolumn=100
highlight ColorColumn ctermbg=DarkGray guibg=DarkGray

