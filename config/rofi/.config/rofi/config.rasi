configuration {
    modi: "window,run,drun";
    combi-modi: "window,run";

    window-format: "{w}    {c}   {t}";
    drun-display-format: "{name} [<span weight='light' size='small'><i>({generic})</i></span>]";

    run-command: "{cmd}";
    ssh-command: "{terminal} {ssh-client} {host} [-p {port}]";
    run-shell-command: "{terminal} {cmd}";
    run-list-command: "";
    window-command: "wmctrl -i -R {window}";

    /* Modes */
    filebrowser {
        /** Directory the file browser starts in. **/
        directory: "/some/directory";
        /**
        * Sorting method. Can be set to:
        *   - "name"
        *   - "mtime" (modification time)
        *   - "atime" (access time)
        *   - "ctime" (change time)
        */
        sorting-method: "name";
        /** Group directories before files. */
        directories-first: true;
    }

    timeout {
        action: "kb-cancel";
        delay:  0;
    }

    /*drun-categories: ; */
    drun-show-actions: false;
    drun-match-fields: "name,generic,exec,categories,keywords";
    drun-use-desktop-cache: false;
    drun-reload-desktop-cache: false;

    lines: 8;
    fixed-num-lines: true;
    columns: 2;
    width: 50;
    eh: 1;
    bw: 5;
    line-margin: 5;
    line-padding: 10;

    location: 0;
    /* padding: 20; TEST */
    m: "-5";
    yoffset: 0;
    xoffset: 0;

    /* theme: "theme"; */ /* [DEPRECATED] */
    /*color-normal: ; */
    /*color-urgent: ; */
    /*color-active: ; */
    /*color-window: ; */

    font: "sans 12";

    terminal: "xdg-terminal";
    drun-url-launcher: "xdg-open";
    ssh-client: "ssh";

    sidebar-mode: false;
    separator-style: "dash";
    display-window: "";
    display-windowcd: "";
    display-run: "";
    display-ssh: "";
    display-drun: "";
    display-combi: "";
    display-keys: "";
    display-file-browser: "";
    /* application-fallback-icon: ; */

    show-icons: false;
    /* icon-theme: ;*/

    sort: false;
    sorting-method: "normal";
    case-sensitive: false;
    auto-select: false;
    /* hover-select: false; */
    /* steal-focus: false; */
    ignored-prefixes: "";
    /* filter: ;*/

    hide-scrollbar: true;
    scroll-method: 1;
    scrollbar-width: 8;
    cycle: true;

    show-match: true;
    matching: "normal";
    normalize-match: false;
    window-match-fields: "all";
    window-thumbnail: false;
    combi-hide-mode-prefix: false;

    disable-history: false;
    max-history-size: 25;

    fake-background: "screenshot";
    fake-transparency: false;

    /* cache-dir: ; */
    parse-hosts: false;
    parse-known-hosts: true;
    fullscreen: false;
    click-to-exit: true;
    threads: 0;
    dpi: -1;
    pid: "/run/user/1000/rofi.pid";
    tokenize: true;

    /* matching-negate-char: '-'; */

    /* => KEYBINDS */
    kb-primary-paste: "Control+V,Shift+Insert";
    kb-secondary-paste: "Control+v,Insert";
    kb-clear-line: "Control+w";
    kb-move-front: "Control+a";
    kb-move-end: "Control+e";
    kb-move-word-back: "Alt+b,Control+Left";
    kb-move-word-forward: "Alt+f,Control+Right";
    kb-move-char-back: "Left,Control+b";
    kb-move-char-forward: "Right,Control+f";
    kb-remove-word-back: "Control+Alt+h,Control+BackSpace";
    kb-remove-word-forward: "Control+Alt+d";
    kb-remove-char-forward: "Delete,Control+d";
    kb-remove-char-back: "BackSpace,Shift+BackSpace,Control+h";
    kb-remove-to-eol: "Control+k";
    kb-remove-to-sol: "Control+u";
    kb-accept-entry: "Control+j,Control+m,Return,KP_Enter";
    kb-accept-custom: "Control+Return";
    /* kb-accept-custom-alt: "Control+Shift+Return"; */
    kb-accept-alt: "Shift+Return";
    kb-delete-entry: "Shift+Delete";
    kb-mode-next: "Shift+Right,Control+Tab";
    kb-mode-previous: "Shift+Left,Control+ISO_Left_Tab";
    /* kb-mode-complete: "Control+l"; */
    kb-row-left: "Control+Page_Up";
    kb-row-right: "Control+Page_Down";
    kb-row-up: "Up,Control+p,ISO_Left_Tab";
    kb-row-down: "Down,Control+n";
    kb-row-tab: "Tab";
    kb-page-prev: "Page_Up";
    kb-page-next: "Page_Down";
    kb-row-first: "Home,KP_Home";
    kb-row-last: "End,KP_End";
    kb-row-select: "Control+space";
    kb-screenshot: "Alt+S";
    kb-ellipsize: "Alt+period";
    kb-toggle-case-sensitivity: "grave,dead_grave";
    kb-toggle-sort: "Alt+grave";
    kb-cancel: "Escape,Control+g,Control+bracketleft";
    kb-custom-1: "";
    kb-custom-2: "";
    kb-custom-3: "";
    kb-custom-4: "";
    kb-custom-5: "";
    kb-custom-6: "";
    kb-custom-7: "";
    kb-custom-8: "";
    kb-custom-9: "";
    kb-custom-10: "";
    kb-select-1: "Alt+1";
    kb-select-2: "Alt+2";
    kb-select-3: "Alt+3";
    kb-select-4: "Alt+4";
    kb-select-5: "";
    kb-select-6: "";
    kb-select-7: "";
    kb-select-8: "";
    kb-select-9: "";
    kb-select-10: "";
    ml-row-left: "ScrollLeft";
    ml-row-right: "ScrollRight";
    ml-row-up: "ScrollUp";
    ml-row-down: "ScrollDown";
    me-select-entry: "MousePrimary";
    me-accept-entry: "MouseDPrimary";
    me-accept-custom: "Control+MouseDPrimary";
}

@theme "theme"

@import "colors.rasi"
