#           _           _          _ _
#   _______| |__    ___| |__   ___| | |
#  |_  / __| '_ \  / __| '_ \ / _ \ | |
#   / /\__ \ | | | \__ \ | | |  __/ | |
#  /___|___/_| |_| |___/_| |_|\___|_|_|

# IMPORT SOURCES
# ------------------
# source..


# EXPORT VARIABLES
# ------------------
# export..


# OPTIONS
# ------------------
set -o emacs
autoload -Uz compinit
compinit -d ~/.cache/zcompdump-$ZSH_VERSION
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zmodload zsh/complist
_comp_options+=(globdots)	# Include hidden files
setopt correct			# Enable auto-correction
setopt autocd			# Automatically cd into typed directory.


# HISTORY
# ------------------
HISTSIZE=3000
SAVEHIST=3000
HISTFILE=~/.cache/zsh_history


# PLUGINS
# ------------------
# [DEPRECATED]
#source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


# DIFFS IN SHELL
# ------------------
# Colorize Man Pages
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'


# DIFFS IN DISTRO
# ------------------
[ -f $HOME/.config/zsh/profile.zsh ] && source $HOME/.config/zsh/profile.zsh


## ALIASES
# ------------------
source "$HOME/.config/shell_alias/shared"
[ -f $HOME/.config/shell_alias/profile ] && source "${HOME}/.config/shell_alias/profile"
