#!/usr/bin/env zsh

# Unlock keyring
if [ -n "$DESKTOP_SESSION" ];then
	eval $(gnome-keyring-daemon --start)
	export SSH_AUTH_SOCK
fi

# Plugins
eval "$(lua /usr/share/z.lua/z.lua --init zsh)"

# Theme
eval "$(starship init zsh)"
