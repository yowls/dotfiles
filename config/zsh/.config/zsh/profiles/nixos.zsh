#!/usr/bin/env zsh

# Plugins
#eval "$(lua ~/Gits/z.lua/z.lua --init zsh)"

source /nix/store/p2jvq3dg9pjmbzilsf6cadyv8zqsz9h3-zsh-autosuggestions-0.7.0/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /nix/store/p21gyfdzb44ayafz6hb518b9z6b14da0-zsh-syntax-highlighting-0.7.1/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Theme
eval "$(starship init zsh)"
