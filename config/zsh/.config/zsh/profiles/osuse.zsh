#!/usr/bin/zsh

# PLUGINS

# Better input
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#=> Pazi - autojump helper
if command -v pazi &>/dev/null; then
  eval "$(pazi init zsh)"
fi

# THEME: Starship prompt
eval "$(starship init zsh)"
