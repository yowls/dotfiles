#!/usr/bin/env zsh

# Plugins
eval "$(lua ~/Gits/z.lua/z.lua --init zsh)"

# Theme
eval "$(starship init zsh)"
