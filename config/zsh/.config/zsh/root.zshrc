# Export variables
# TODO: set if unset
export VISUAL="/usr/bin/nvim"
export EDITOR="/usr/bin/nvim"

# Options
set -o emacs
autoload -Uz compinit
compinit -d ~/.cache/zcompdump-$ZSH_VERSION
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zmodload zsh/complist
_comp_options+=(globdots)	# Include hidden files
setopt correct			# Enable auto-correction
setopt autocd			# Automatically cd into typed directory.

# History
HISTSIZE=3000
SAVEHIST=3000
HISTFILE=~/.cache/zsh_history

# Theme
autoload -U colors && colors	# Load colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

# Alias
alias grupdate="grub-mkconfig -o /boot/grub/grub.cfg"
alias gruconf="$EDITOR /etc/default/grub"
alias p="ps -el | grep"
alias rswap="swapoff -a && sudo swapon -a"
alias lsbok="lsblk -o name,mountpoint,label,size,uuid"
alias partid="blkid -s UUID -o value"
alias zconf="$EDITOR ~/.zshrc"
alias nvconf="$EDITOR ~/.config/nvim/init.vim"

# Put Colors
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias dd='dd status=progress'

# Upgrade Man-page with colors
function man() {
    env \
    LESS_TERMCAP_mb=$'\e[01;31m'	\
    LESS_TERMCAP_md=$'\e[01;31m'	\
    LESS_TERMCAP_me=$'\e[0m'		\
    LESS_TERMCAP_se=$'\e[0m'		\
    LESS_TERMCAP_so=$'\e[01;44;33m'	\
    LESS_TERMCAP_ue=$'\e[0m'		\
    LESS_TERMCAP_us=$'\e[01;32m'	\
    man "$@"
}

alias ls="ls -hN --color=auto --group-directories-first"
alias la="ls -a --color=auto --group-directories-first"
alias ll="ls -la --color=auto --group-directories-first"
alias l="ls -l --color=auto --group-directories-first"
alias vi="nvim"
alias vim="nvim"

# Pluggins
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
