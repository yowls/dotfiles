#+TITLE: NixOs README
#+DESCRIPTION: The guide to set-up my nixos config

* 📖 ABOUT
- This is my setup for nixos using the nix language to declare all the system settings.
- This configuration uses 'Flakes' and 'Home-Manager' to build the configuration.
- I'm using home-manager in Standalone mode.


* 🚀 GETTING STARTED
- Move your "hardware.nix" to host/<your_host>/hardware.nix
- Build the new configuration as follow:
  - TODO make org button to run shortcuts
  #+BEGIN_SRC bash
  # Set variables
  HOST="ideapad520"
  USER="yowls"

  # Build config
  sudo nixos-rebuild switch --flake .#$HOST
  home-manager switch --flake .#$USER

  # Create links
  ln -s hosts/$HOST current-host
  ln -s users/$USER current-user
  #+END_SRC


* 🖼️ SCREENSHOTS
- TODO


* 📑 DOCUMENTATION
** PROFILES
*** Hosts
- shared:
- ideapad520:
*** Users
- shared:
- yowls:
** ON NON-NIXOS
- install nix with the script of the page
- install home-manager using nix
** UPDATING
- adding the new channel
- nix flake update


* 📚 REFERENCE
- [[https://nixos.org/learn.html][Nixos-Learn]]
