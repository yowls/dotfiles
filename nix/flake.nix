{
  description = "Yowls NixOs Config";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
    home-manager = {
      url = "github:nix-community/home-manager/release-22.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, home-manager, ... }:
    let
      overlays = [
        (self: super: {
          discord = super.discord.overrideAttrs (
            _: { src = builtins.fetchTarball https://discord.com/api/download?platform=linux&format=tar.gz; }
          );
        })

        # (import ./overlays/qtile.nix)
      ];

    in {
      nixosConfigurations = {
        ideapad520 = import ./hosts/ideapad520 {
          inherit nixpkgs overlays;
        };
      };

      homeConfigurations = {
        yowls = import ./users/yowls {
          inherit home-manager nixpkgs overlays;
        };
      };
    };
}
