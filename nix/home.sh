#!/usr/bin/env bash

USER = "yowls" #TODO: replace with user option

# CHANNELS
# nix-channel -update

# BUILD CONFIG
# home-manager switch \
# 	-f ./$profile/home.nix
home-manager switch --flake .#$USER --impure
