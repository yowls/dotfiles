{ pkgs, ... }:

{
  networking = {
    hostName = "ideapad520";

    # PROXY
    # proxy.default = "http://user:password@proxy:port/";
    # proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  };


  services.xserver = {
    # X11 WINDOW SYSTEM
    enable = true;
    videoDrivers = [ "intel" ];

    # X11 KEYMAP
    layout = "es";
    # xkbOptions = "eurosign:e";

    # X11 TOUCHPAD
    # Disable libinput driver and use the Synaptic driver instead
    libinput.enable = false;
    synaptics = {
      enable = true;
      tapButtons = true;
      minSpeed = "0.65";
      maxSpeed = "1.0";
      accelFactor = "0.25";
      vertTwoFingerScroll = true;
      horizTwoFingerScroll = true;
      vertEdgeScroll = true;
      palmDetect = false;
      additionalOptions = ''
        Option      "VertScrollDelta"          "-111"
        Option      "HorizScrollDelta"         "-111"
      '';
      buttonsMap = [ 1 3 2 ];
      fingersMap = [ 1 3 2 ];
    };

    # DESKTOP ENVIRONMENT: GNOME
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;

    # WINDOW MANAGER
    windowManager.qtile.enable = true;
  };
  # xdg.portal.enable = true # Required to use flatpak if not using Gnome
  services.ananicy = {
    enable = true;
    # settings = {}
  };
  services.ratbagd.enable = true;


  # AUTO UPDATES
  # NOTE: Remember update the channel
  # system.autoUpgrade = {
  #   enable = true;
  #   channel = https://nixos.org/channels/nixos-22.05;
  # };


  # PACKAGES
  # TODO: move to hardware
  boot.kernelPackages = pkgs.linuxKernel.packages.linux_xanmod;

  environment.systemPackages = with pkgs; [
    # LAPTOP THINGS
    # bluez bluez-tools
    # tlp
    # light

    # CODECS AND DRIVERS
    microcodeIntel
    btrfs-progs f2fs-tools xfsprogs dosfstools mtools
    gparted
    # ffmpeg
    unrar unzip
    cryptsetup

    # X11
    xorg.xf86videointel xorg.xf86inputsynaptics
    xclip
    # redshift

    # WAYLAND
    # gammastep

     # SOUND
    pavucontrol

    # LOOK AND FEEL
    # lxappearance
    bibata-cursors
    # plymouth

    # GNOME THINGS
    deja-dup
    gnome.gnome-tweaks
    gnomeExtensions.tray-icons-reloaded
    gnomeExtensions.clipboard-indicator
    gnomeExtensions.gsconnect
    gnomeExtensions.night-theme-switcher
    gnomeExtensions.blur-my-shell
  ];

  virtualisation.docker.enable = true;
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;


  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11";
}
