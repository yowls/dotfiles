{ nixpkgs, overlays }:

nixpkgs.lib.nixosSystem rec {
  system = "x86_64-linux";

  modules = [
    {
      nixpkgs = {
        inherit overlays;
        config.allowUnfree = true;
      };
    }
    ./configuration.nix
    ./hardware.nix
    ../shared
  ];
}
