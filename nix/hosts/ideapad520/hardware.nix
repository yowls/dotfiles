{ config, lib, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  # KERNEL MODULES
  boot = {
    initrd.availableKernelModules = [ "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
    initrd.kernelModules = [ "dm-snapshot" ];
    kernelParams = [ "quiet" ];
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [ ];
  };

  #########################################################

  # ROOT
  fileSystems."/" =
    { device = "/dev/disk/by-uuid/bd905523-6b26-4ddc-83da-92908cb3f4d7";
      fsType = "f2fs";
      options= ["compress_algorithm=zstd"];
    };

  # BOOT
  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/CB3B-D94A";
      fsType = "vfat";
    };

  #########################################################

  # -- External drives

  # BACKUPS (/dev/sdb1) (lvm)
  fileSystems."/backup/configs" =
    { device = "/dev/disk/by-uuid/7c49702c-8038-4cda-9548-55eb01acf41e";
      fsType = "btrfs";
      options= [ "rw" "noauto" "user" "async" "noatime""compress=zstd:4" "space_cache=v2" ];
    };
  fileSystems."/backup/services" =
    { device = "/dev/disk/by-uuid/3f86e415-e151-4897-a381-c4b3ceb1be64";
      fsType = "btrfs";
      options= [ "rw" "auto" "user" "async" "noatime""compress=zstd:4" "space_cache=v2" ];
    };
  fileSystems."/backup/remotes" =
    { device = "/dev/disk/by-uuid/38b3b9ae-cffe-40ad-ade8-08cc158632fb";
      fsType = "xfs";
      options= [ "defaults" "auto" "user" ];
    };
  fileSystems."/backup/virt" =
    { device = "/dev/disk/by-uuid/8e3483e9-83da-4db7-8a9f-562f18a24cb9";
      fsType = "xfs";
      options= [ "defaults" "noauto" "user" ];
    };
  fileSystems."/backup/distros" =
    { device = "/dev/disk/by-uuid/6769c043-4a26-4592-b1dd-530c913fe458";
      fsType = "btrfs";
      options= [ "rw" "auto" "user" "async" "noatime" "compress=zstd:3" "space_cache=v2" "subvol=@nixos" ];
    };

  # > ENIGMA (/dev/sdb3)
  fileSystems."/enigma" =
    { device = "/dev/disk/by-uuid/4a2d214a-8d78-4414-96ec-782ac188f211";
      fsType = "ext4";
      options= [ "rw" "noauto" "user" "async" "errors=remount-ro" ];
    };

  # > LIBRARY (/dev/sdb5)
  fileSystems."/home/yowls/library" =
    { device = "/dev/disk/by-uuid/4ca6443a-d2c1-4c64-9973-bba4e8a1bbb0";
      fsType = "ext4";
      options= [ "rw" "auto" "user" "async" "errors=continue" ];
    };

  #########################################################

  # SWAP
  # swapDevices = [ ];

  # Zram
  zramSwap = {
    enable = true;
    memoryPercent = 80;
    swapDevices = 1;
    priority = 10;
    algorithm = "zstd";
  };

  #########################################################

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  #########################################################

  # BLUETOOTH
  hardware.bluetooth.enable = true;
}
