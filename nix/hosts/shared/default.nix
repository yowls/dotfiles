{ pkgs, ... }:

let
  hostsSteveblack = pkgs.fetchurl {
    url = "https://raw.githubusercontent.com/StevenBlack/hosts/3.11.29/hosts";
    sha256 = "sha256-gVmbCwqrmTbDd5lq6E9S8WIlVvE8SBlEYMx1gETaSbI=";
  };

in
{
  boot = {
    # Systemd-Boot
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;

    # PLYMOUTH
    # plymouth.enable = true;
    # plymouth.theme = "spinner";

    # CLEAN /TMP
    tmp.useTmpfs = true;
    # tmp.cleanOnBoot = true;
  };


  # NIX CONFIG
  nix = {
    # ENABLE FLAKES
    package = pkgs.nixFlakes;
    extraOptions = "experimental-features = nix-command flakes";

    # AUTO CLEAN PACKAGES
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
    settings.auto-optimise-store = true;
  };

  # ALLOW UNFREE PACKAGES
  # nixpkgs.config.allowUnfree = true;


  services = {
    openssh.enable = true;
    flatpak.enable = true;
    # printing.enable = true;

    pipewire = {
      enable = true;
      pulse.enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
    };
  };

  security.rtkit.enable = true;
  hardware.pulseaudio.enable = false;


  networking = {
    # Network Manager
    networkmanager.enable = true;

    # DHCP
    useDHCP = false;
    interfaces.enp1s0.useDHCP = true; # REVIEW: Sould this be part of hardware?
    interfaces.wlp2s0.useDHCP = true; # REVIEW: Sould this be part of hardware?

    # Network rules
    firewall = {
      enable = true;
      allowedTCPPorts = [ 80 443 ];
      # allowedUDPPorts = [ ... ];
      allowedTCPPortRanges = [{ from = 1714; to = 1764; }]; # kdeconnect
      allowedUDPPortRanges = [{ from = 1714; to = 1764; }]; # kdeconnect
    };

    # Hosts file
    extraHosts = '' ${builtins.readFile hostsSteveblack} '';
  };


  # TIME ZONE AND LANGUAGE
  time.timeZone = "America/Santiago";
  i18n.defaultLocale = "en_US.UTF-8";


  # TTY
  console = {
    font = "Lat2-Terminus16";
    keyMap = "es";
  };


  # ENVIRONMENT
  # TODO: move to user profile
  # environment.sessionVariables = { };
  # environment.variables = {
    # XDG_CONFIG_HOME = "$HOME/.config";
    # XDG_CACHE_HOME  = "$HOME/.cache";
    # XDG_DATA_HOME   = "$HOME/.local/share";

    # Settings
    # TERMINAL = "kitty";
    # VISUAL   = "/run/current-system/sw/bin/nvim";
    # EDITOR   = "/run/current-system/sw/bin/nvim";
    # BROWSER  = "/run/current-system/sw/bin/firefox";

    # Custom PATH
    # PATH = "$PATH:$HOME/bin:$HOME/.local/bin/";

    # Applications
    # MPD_HOST = "$HOME/.config/mpd/socket";
  # };


  # USER
  users.users = {
    yowls = {
      home = "/home/yowls";
      shell = pkgs.fish;
      isNormalUser = true;
      extraGroups = [ "wheel" "networkmanager" "docker" "libvirtd" ];
    };
  };
  programs.fish.enable = true;


  # FONTS
  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    fira-code
    fira-code-symbols

    (nerdfonts.override {
      fonts = [
        "IBMPlexMono"
        "DaddyTimeMono"
        "DroidSansMono"
        "Inconsolata"
        "Iosevka"
        "JetBrainsMono"
        "Lekton"
        "Monofur"
        "Monoid"
        "Mononoki"
        "ProggyClean"
      ];
    })
  ];
}
