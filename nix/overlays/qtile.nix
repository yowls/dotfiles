self: super:
let
  pkgs = super.pkgs;
  version = "0.18.1";
in {
  qtile = pkgs.lib.overrideAttrs (oldAttrs: {
    inherit version;
    pname = "qtile-${version}";
    src = pkgs.fetchFromGitHub {
      owner = "qtile";
      repo = "qtile";
      rev = "v${version}";
      sha256 = "0q3ap6an3vb6sikbqw9ml7ss4p4f4i7l4qx74wvlzmzpmvihddbc";
    };
  }) super.qtile;
}

# TODO: add python deps
#       dbus-next
#       psutil
