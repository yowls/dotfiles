{
  fs = "no";
  geometry = "50%:50%";
  autofit-larger = "60%x60%";
  profile = "gpu-hq";
  volume = 25;
  audio-device = "alsa/default";
  sub-font-size = 30;
  sub-bold = "yes";
  sub-auto = "fuzzy";
  screenshot-format = "png";
  screenshot-directory = "/home/yowls/Pictures";
}
