require('mini.comment').setup()
require('mini.completion').setup()
require('mini.cursorword').setup()
require('mini.indentscope').setup()
require('mini.pairs').setup()
require('mini.surround').setup()
require('mini.trailspace').setup()


