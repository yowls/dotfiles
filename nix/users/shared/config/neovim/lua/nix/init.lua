-- PRELOAD
require('impatient')

-- LOAD THE CONFIG
require('nix.options')
require('nix.keymap')
require('nix.plugins')
