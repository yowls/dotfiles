---
-- NEOVIM GENERAL CONFIG
---

-- SHORTCUTS
local o = vim.o
local g = vim.g


-- Syntax
o.foldmethod = "syntax"

-- Search
o.ignorecase	= true
o.smartcase	= true
o.hlsearch	= true
o.incsearch	= true
o.showmatch	= true
o.inccommand	= "split"

-- Indentation
-- expandtab	= false
o.autoindent		= true
o.smartindent		= true
-- smarttab	= true
o.tabstop			= 4
o.shiftwidth		= 4
o.softtabstop		= 0
o.shiftround		= true

-- Line
o.number			= true
o.numberwidth		= 2
o.relativenumber	= true
o.wrap			= true
o.linebreak		= true
o.list			= true
o.listchars  = "trail:×,nbsp:◇,tab:⇒ ,space:·,extends:▸,precedes:◂"

-- Cursor
o.cursorline		= true
o.cursorcolumn	= true
o.scrolloff		= 2

-- Colors
g.loaded_netrw = 1
g.loaded_netrwPlugin = 1
o.termguicolors	= true
o.colorcolumn		= "80"

-- Buffers
o.hidden			= true
o.splitright		= true
o.splitbelow		= true
o.sidescrolloff	= 8

-- Swap file
-- o.swapfile	= false

-- Menu
-- o.completeopt	= "menu,menuone,noselect"
o.completeopt		= "menu,preview"

-- Various
o.mouse		= "a"
o.clipboard	= "unnamedplus"
o.backspace	= "indent,eol,start"
o.undofile	= true
o.undodir		= vim.fn.stdpath('cache') .. '/nvim-undodir'

vim.cmd [[au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif"]]

-- File type
vim.cmd("autocmd FileType scss setl iskeyword+=@-@")
vim.cmd("autocmd FileType haskell setl expandtab")

