---
-- NEOVIM PLUGINS
---
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end


require('packer').startup(function(use)
  -- Essential
  use "wbthomason/packer.nvim"
  use "lewis6991/impatient.nvim"

  -- Theme
  use "folke/tokyonight.nvim"

  -- Bars and Trees
  use {
    "nvim-lualine/lualine.nvim",
    requires = { 'kyazdani42/nvim-web-devicons', opt = true },
  }
  use {
    'nvim-tree/nvim-tree.lua',
    requires = {'nvim-tree/nvim-web-devicons'},
  }

  -- Syntax
  use "LnL7/vim-nix"
  use "norcalli/nvim-colorizer.lua"
  use({'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'})

  -- Tools and other Modules
  use "lewis6991/gitsigns.nvim"
  use {"echasnovski/mini.nvim", branch = 'stable' }
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.0',
    requires = { {'nvim-lua/plenary.nvim'} }
  }

  -- Automatically set up your configuration after cloning packer.nvim
  if ensure_packer() then
    require('packer').sync()
  end
end)


vim.cmd[[colorscheme tokyonight]]
