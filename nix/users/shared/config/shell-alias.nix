{
# 1 - System
#=================
  suconf = "sudo EDITOR=$EDITOR visudo";
  gruconf = "sudo $EDITOR /etc/default/grub";

# Processes
  p = "ps -el | grep";
  kk = "kill -s USR1";
  ka = "killall $1 || true";
  pk = "pkill $1 || true";

# Others
  ram = "free -h --mega";
  hosts = "sudo $EDITOR /etc/hosts";
  rswap = "sudo swapoff -a && sudo swapon -a";
  lsbok = "sudo lsblk -o name,mountpoint,label,size,uuid";
  partid = "blkid -s UUID -o value";
  openluks = "sudo cryptsetup luksOpen";
  closeluks = "sudo cryptsetup luksClose";

# Put Colors
  grep = "grep --color=auto";
  egrep = "egrep --color=auto";
  fgrep = "fgrep --color=auto";
  dd = "dd status=progress";

# Encrypt text file (need parameters)
  crypt = "openssl enc -aes-256-cbc -md sha512 -pbkdf2 -iter 100000 -salt -in $1 -out $1.enc";
  uncrypt = "openssl enc -aes-256-cbc -md sha512 -pbkdf2 -iter 100000 -salt -d -in $1 -out $1-decrypt";


# 2 - Package manager
#======================
  fpls = "flatpak list --app";
  fpss = "flatpak search";
  fpin = "flatpak install";
  fprm = "flatpak uninstall";
  fprmf = "flatpak uninstall --delete-data";
  fpcl = "flatpak uninstall --unused";


# 3 - Configuration files | Dotfiles
#=====================================
# BASH SHELL
  cdba = "cd ~/.bash.d";
  baconf = "$EDITOR ~/.bashrc";

# FISH SHELL
  cdfi = "cd ~/.config/fish";
  ficonf = "$EDITOR ~/.config/fish/config.fish";

# Z SHELL
  cdz = "cd ~/.config/zsh";
  zconf = "$EDITOR ~/.config/zsh/.zshrc";

# ALIAS
  cdshk = "cd ~/.alias.d";
  shk = "$EDITOR ~/.alias.d/shell";
  shkk = "$EDITOR ~/.alias.d/$DISTRO";

# XRESOURCES
  cdxr = "cd ~/.Xresources.d";
  xrfd = "$EDITOR ~/.Xresources.d";
  xrconf = "$EDITOR ~/.Xresources";
  rxr = "xrdb -merge ~/.Xresources";

# TERMINAL KITTY
  cdki = "cd ~/.config/kitty";
  kifd = "$EDITOR ~/.config/kitty";
  kiconf = "$EDITOR ~/.config/kitty/kitty.conf";
  kikey = "$EDITOR ~/.config/kitty/keybinds.conf";

# EDITORS
  nvfd = "nvim ~/.config/nvim";

# OTHERS
  todo = "emacs ~/Cloudport/$DISTRO.org";	#[DEPRECATED]


# 4 - Common Directories
#============================
  cdd = "cd ~/Downloads";
  cddc = "cd ~/Documents";
  cdb = "cd ~/bin";
  cdg = "cd ~/Gits";

# DOTFILES
  cddt = "cd ~/.dotfiles";
  cddtn = "cd ~/.dotfiles/nixos";

# SCRIPTS
  cdsc = "cd $HOME/scripts";
  scfd = "$EDITOR $HOME/scripts";
  scmd = "$EDITOR $HOME/scripts/README.md";

# STARTPAGES
  cdsp = "cd $HOME/startpages";
  spfd = "$EDITOR $HOME/startpages";
  spmd = "$EDITOR $HOME/startpages/README.md";

# FIREFOX
  cdf = "cd ~/.mozilla/firefox/*.default-release/chrome";
  ffd = "$EDITOR ~/.mozilla/firefox/*.default-release/chrome/";
  fcss = "$EDITOR ~/.mozilla/firefox/*.default-release/chrome/userChrome.css";
  fcnt = "$EDITOR ~/.mozilla/firefox/*.default-release/chrome/userContent.css";
  fmd = "$EDITOR ~/.mozilla/firefox/*.default-release/chrome/README.md";

# EMACS
  cdem = "cd ~/.emacs.d";
  emfd = "emacs ~/.emacs.d";
  emmd = "emacs ~/.emacs.d/README.org";


# 5 - Aplications
#=====================
  wttr = "curl wttr.in";

# Exa
  ls = "exa --group-directories-first -x";
  la = "exa --group-directories-first -ax --icons";
  lt = "exa --group-directories-first -a --icons --tree";
  ll = "exa --group-directories-first -lha --icons --git";
  l = "exa --group-directories-first -lh --git";

# GIT
  gig = "$EDITOR .gitignore";
  rmd = "$EDITOR README.md";
  glogg = "git log --all --graph --decorate --oneline";
  gst = "git status";
  gaa = "git add -A";
  gpl = "git pull";
  gps = "git push";

# Youtube downloader (yt-dlp)
  audio = "yt-dlp -x -f bestaudio --audio-format best";
  mp3 = "yt-dlp -x --audio-format mp3";
  mp3t = "yt-dlp -x --audio-format mp3 --embed-thumbnail";
  video = "yt-dlp -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4";

# HBlock (hosts) [DEPRECATED]
  # uhblock = "hblock --sources $HOME/library/Hosts/sources --allowlist $HOME/library/Hosts/allow.list --denylist $HOME/library/Hosts/deny.list";


# 6 - Common errors
#======================
  vi = "nvim";
  vim = "nvim";


# 7 - NIXOS
#---- 1.System
# ..
#---- 2.Package manager
#---2.1 SEARCH
# Search in online repositories
  nixss = "nix search";
# Search local packages
  nixssl = "nix search";
# Search for package info
  nixinf = "nix-env -qa --description $1";
# Search for package files
  nixfiles = "du -a $(readlink -f $(which $1))";

#---2.2 INSTALL
# Istall from remote repository
  nixin = "nix-env --install";
# Install from local package
  nixinl = "echo \?\?";
# Download a package
  nixdw = "echo \?\?";

#---2.3 REMOVE
# Remove a package
  nixrm = "nix-env --uninstall";
# Remove completly include dependencies
  nixrmf = "echo \?";
# Remove unneeded dependecy
  nixnd = "echo \?";
# Remove old downloaded packages
  nixcl = "nix-collect-garbage -d";

#---2.4 UPDATE
# Update repositories database
  nixu = "sudo nix-channel --update";
  update = "sudo nix-channel --update";
# Upgrade packages
  nixug = "sudo nixos-rebuild boot --show-trace";
  upgrade = "sudo nixos-rebuild boot --show-trace; nix-env -u; flatpak update";
# List avaible upgrades
  nixuls = "nixos-rebuild dry-run --upgrade";
  upgradels = "nixos-rebuild dry-run --upgrade";
# Update and Upgrade
  nya = "update && upgradels";
  nyya = "update && upgrade";

#---- 3.Configuration files | dotfiles
#---3.1 DE
  destart = "nvim ~/.config/autostart/scripts";
#---3.2 BERRY
  cdbr = "cd ~/.config/berry";
  brfd = "nvim ~/.config/berry";
  brconf = "nvim ~/.config/berry/autostart";
  brstart = "nvim ~/.config/berry/startup.sh";
  brkey = "nvim ~/.config/berry/keybind.sh";
  brmd = "nvim ~/.config/berry/README.md";
#---3.3 STUMPWM
  cdst = "cd ~/.stumpwm.d/";
  stfd = "emacs ~/.stumpwm.d/";
  stconf = "emacs ~/.stumpwm.d/init.lisp";
  ststart = "nvim ~/.stumpwm.d/autostart.sh";
  stkey = "emacs ~/.stumpwm.d/keybind.lisp";
  strd = "emacs ~/.stumpwm.d/README.org";
#---3.4 XMonad
  cdxm = "cd ~/.xmonad";
  xmfd = "nvim ~/.xmonad";
  xmconf = "nvim ~/.xmonad/xmonad.hs";
  xmlib = "nvim ~/.xmonad/lib/";
  xmset = "nvim ~/.xmonad/lib/settings/";
  xmrd = "nvim ~/.xmonad/README.org";

#[DEPRECATED]
#---3.5 LEMONBAR
  cdlb = "cd ~/.config/lemonbar";
  lbfd = "nvim ~/.config/lemonbar";
  lbmd = "nvim ~/.config/lemonbar/README.md";

#---- 4.Applications
#..
}
