{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "yowls";
  home.homeDirectory = "/home/yowls";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.05";

  # PACKAGES
  home.packages = with pkgs; [
    # firefox
    vivaldi

    emacs
    vscodium
    tilda

    gimp
    inkscape

    neofetch
    #bpytop
    exa

    keepassxc
    # megasync
    vlc

    # ananicy-cpp
    bleachbit
    #flutter
    #android-studio
  ];

  # CONFIGS
  programs = {
    # Let Home Manager install and manage itself.
    home-manager.enable = true;

    # z-Shell
    zsh = {
      enable = true;
      enableAutosuggestions = true;
      # enableCompletion = true;
      enableSyntaxHighlighting = true;
      autocd = true;
      completionInit = "autoload -Uz compinit && compinit -d ~/.cache/zcompdump-$ZSH_VERSION";
      defaultKeymap = "emacs";
      dotDir = ".config/zsh";
      history = {
        expireDuplicatesFirst = true;
        path = "$HOME/.cache/.zsh_history";
        save = 3000;
        size = 3000;
      };
      # sessionVariables = {};
      initExtra = ''
        export LESS_TERMCAP_mb=$'\e[1;32m'
        export LESS_TERMCAP_md=$'\e[1;32m'
        export LESS_TERMCAP_me=$'\e[0m'
        export LESS_TERMCAP_se=$'\e[0m'
        export LESS_TERMCAP_so=$'\e[01;33m'
        export LESS_TERMCAP_ue=$'\e[0m'
        export LESS_TERMCAP_us=$'\e[1;4;31m'

        zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
      '';
      shellAliases = import config/shell-alias.nix;
    };
    pazi = {
      enable = true;
      enableZshIntegration = true;
    };
    starship = {
      enable = true;
      enableZshIntegration = true;
      # settings = {}
    };

    # Git
    # git = {}
  };
}
