{ home-manager, nixpkgs, overlays }:

# {
  # DEPRECATED
  # USER
  # Don't forget to set a password with ‘passwd’.
  # users.users.yowls = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" "networkmanager" "docker" "libvirtd" ];
  # };
  # SHELL
  # programs.fish.enable = true;
  # users.defaultUserShell = pkgs.fish;
  # PACKAGES
  # virtualisation.docker.enable = true;
  # virtualisation.libvirtd.enable = true;
  # programs.dconf.enable = true;
# }

let
  system = "x86_64-linux";

in
  home-manager.lib.homeManagerConfiguration rec {
    pkgs = nixpkgs.outputs.legacyPackages.${system};
    modules = [
      {
        nixpkgs = {
          inherit overlays;
          config.allowUnfree = true;
        };
        home = rec {
          username = "yowls";
          homeDirectory = "/home/${username}";
          stateVersion = "22.11";
        };
      }

      ./home.nix
      # ../shared/home.nix
    ];
  }
