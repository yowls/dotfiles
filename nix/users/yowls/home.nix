{ config, pkgs, ... }:

{
  home.username = "yowls";
  home.homeDirectory = "/home/yowls";


  # ENVIRONMENT
  home = {
    sessionPath = [
      "${config.home.homeDirectory}/.local/bin"
    ];

    sessionVariables = {
      # TERMINAL = "${pkgs.kitty}/bin/kitty";
      VISUAL   = "${pkgs.neovim}/bin/nvim";
      EDITOR   = "${pkgs.neovim}/bin/nvim";
      BROWSER  = "${pkgs.firefox}/bin/firefox";

      # Applications
      QT_QPA_PLATFORMTHEME = "qt5ct";
      MPD_HOST = "${config.xdg.configHome}/mpd/socket";
      # TRILIUM_DATA_DIR = "/backup/services/trilium/src";
    };
  };

  xdg = {
    enable = true;
    cacheHome  = "${config.home.homeDirectory}/.cache";
    configHome = "${config.home.homeDirectory}/.config";
    dataHome   = "${config.home.homeDirectory}/.local/share";

    configFile = {
      "nvim" = {
        recursive = true;
        source = ../shared/config/neovim;
      };
      "doom" = {
        recursive = true;
        source = ../shared/config/doom-emacs;
      };
    };
  };

  # gtk = {};


  # SERVICES
  services.gpg-agent = {
    enable = true;
    # defaultCacheTtl = 1800;
    enableSshSupport = true;
  };

  # PACKAGES
  home.packages = with pkgs; [
    # LANGUAGES
    nodejs nodePackages.pyright
    gcc
    lua5_1
    black

    # WINDOW MANAGER THINGS (X11)
    picom # flashfocus
    feh hsetroot # xwinwrap
    dunst
    rofi # greenclip
    kitty rxvt-unicode
    mpd mpc_cli ncmpcpp playerctl
    maim
    xclip
    redshift
    eww
    # xmenu xclickroot
    light

    # TERMINAL THINGS
    neovim
    wget curl
    rsync rclone
    htop exa bat
    neofetch
    yt-dlp
    # glab # Gitlab CLI
    distrobox

    # APPLICATIONS GUI
    # tilda flameshot albert
    emacs fd ripgrep
    vlc gimp imagemagick
    keepassxc
    transmission-gtk megasync
    firefox brave
    virt-manager
    obs-studio
    discord
    piper

    calibre
    joplin-desktop logseq #trilium-desktop
    # mangohud goverlay vkBasalt
  ];


  # PACKAGE CONFIGS
  programs = {
    # Let Home Manager install and manage itself.
    home-manager.enable = true;

    # SHELL
    fish = {
      enable = true;
      interactiveShellInit = ''
         set fish_greeting
         set -x LESS_TERMCAP_mb (printf "\033[01;31m")
         set -x LESS_TERMCAP_md (printf "\033[01;31m")
         set -x LESS_TERMCAP_me (printf "\033[0m")
         set -x LESS_TERMCAP_se (printf "\033[0m")
         set -x LESS_TERMCAP_so (printf "\033[01;44;33m")
         set -x LESS_TERMCAP_ue (printf "\033[0m")
         set -x LESS_TERMCAP_us (printf "\033[01;32m")
      '';
      shellAliases = import ../shared/config/shell-alias.nix;
    };
    z-lua = {
      enable = true;
      enableFishIntegration = true;
    };
    starship = {
      enable = true;
      enableFishIntegration = true;
    };

    # EDITORS
    # neovim = {
    #   enable = true;
    #   viAlias = true;
    #   vimAlias = true;
    # };

    git = {
      enable = true;
      userName = "Yowls";
      userEmail = "enrique.lnx@gmail.com";
      signing = {
        key = "D19E05F4C4008F1D";
        signByDefault = true;
      };
      delta.enable = true;
    };

    # DOWNLOADER
    # TODO: move to shared
    # yt-dlp = {
    #  enable = true;
    #  extraConfig = ''
    #    --audio-quality 0
    #    -o '$HOME/Downloads/%(title)s.%(ext)s'
    #    --embed-metadata
    #    --ignore-errors
    #    --progress
    #    --verbose
    #  '';
    #  settings = "";
    #};

    # VIDEO/MUSIC PLAYER
    mpv = {
      enable = true;
      config = import ../shared/config/mpv/config.nix;
      bindings = import ../shared/config/mpv/bindings.nix;
    };
  };


  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";
}
