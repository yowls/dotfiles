# 📚 Some guides
a description

<br>
<br>

---

### Xorg configs
+ [set the keyboard](xorg/00-keyboard.conf)
+ [set the monitor](xorg/10-monitor.conf)
+ [set video driver](xorg/20-intel.conf)
+ [set touchpad synaptic](xorg/70-synaptics.conf)

<br>

### Mounting Drives
+ [fstab](fstab)
+ [crypttab](crypttab)
+ [Virt Manager](virt_manager)

<br>

### I fucked the boot
+ [efi](recover-efi)
+ [grub](recover-grub)

<br>

### Distro package manager config
+ [debian source list](debian.list)
+ [arch pacman](pacman.conf)

<br>

### Scripts
+ [xrandr]
+ [cron tab]
