# 📦 Some fixes for flatpak apps

## Fix permissions:
- Install: [flatsteal](https://flathub.org/apps/details/com.github.tchx84.Flatseal)

TODO: add description to enable printer

<br>

## Firefox
### Video rendering with lag
- Find:
``` bash
$ ~/.local/share/flatpak/app/org.mozilla.firefox/current/active/metadata # user
$ /var/lib/flatpak/app/.. # TODO add system installation path
```

- The file should have something like this:
``` conf
[Extension org.freedesktop.Platform.ffmpeg-full]
directory=lib/ffmpeg
add-ld-path=.
no-autodownload=true
version=21.08
```

- Therefore, you should install ffmpeg version 21.08:
``` bash
$ flatpak install org.freedesktop.Platform.ffmpeg-full/x86_64/21.08
```

### Bad Font rendering
- Find the file: (create directories if doesn't exists)
``` bash
$ ~/.var/app/org.mozilla.firefox/config/fontconfig/fonts.conf
```

- Write the following:
``` conf
<?xml version='1.0'?>
<!DOCTYPE fontconfig SYSTEM 'fonts.dtd'>
<fontconfig>
    <!-- Disable bitmap fonts. -->
    <selectfont><rejectfont><pattern>
        <patelt name="scalable"><bool>false</bool></patelt>
    </pattern></rejectfont></selectfont>
</fontconfig>
```
