#!/bin/env bash

#
# DESCRIPTION
#
# This script install some frequently used software
# Require some additionals steps for a full-setup


#
# INSTALL SOFTWARE FROM OFFICIAL REPOSITORIES
#
sudo pacman -S git stow zsh neovim gufw kitty mpd mpc mpv ncmpcpp \
                pavucontrol rsync rclone timeshift gparted flameshot tilda imagemagick \
                exa emacs hblock


#
# ADITIONAL
#

# PARU AUR HELPER

sudo pacman -S --needed base-devel
mkdir ~/git
git clone https://aur.archlinux.org/paru.git ~/git/paru
cd ~/git/paru
makepkg -si
