#!/bin/bash

#
# DESCRIPTION
#
# This script install some frequently used software
# This avoid things about: WM, DE and Theming related
# Require some additionals steps for a full-setup


#
# INSTALL SOFTWARE FROM APT
#
sudo apt install flatpak git stow aptitude fish neovim gufw python-is-python3 cryptsetup \
                pavucontrol rsync rclone timeshift gparted flameshot tilda imagemagick unrar


#
# FROM UBUNTU 22.04 OR LATER
#
sudo apt install exa emacs-gtk


#
# INSTALL SOFTWRE FROM FLATPAK
#
flatpak install flathub com.discordapp.Discord    \ # discord
                        org.telegram.desktop      \ # telegram
                        com.vscodium.codium       \ # vscodium
                        com.todoist.Todoist       \ # todoist
                        com.logseq.Logseq         \ # logseq
                        net.cozic.joplin_desktop  \ # joplin
                        org.keepassxc.KeePassXC   \ # keepassxc
                        com.github.tchx84.Flatseal  # flatsteal


#
# ADITIONAL
#

# HBLOCK
curl -o /tmp/hblock 'https://raw.githubusercontent.com/hectorm/hblock/v3.3.2/hblock' \
  && echo '864160641823734378b69da7aa28477771e125af66cf47d5f0f7c8233ef1837f  /tmp/hblock' | shasum -c \
  && sudo mv /tmp/hblock /usr/local/bin/hblock \
  && sudo chown 0:0 /usr/local/bin/hblock \
  && sudo chmod 755 /usr/local/bin/hblock


#
# TODO: ADD FONTS
#
