# 📘 Virt manager
a little guide to remember some things

<br>

---

<br>

## 💽 Share Folder
+ I usually set the ***~/Public*** folder as the share folder on the host machine<br>
+ For the VM, i usually set ***/share*** as the share point<br>
+ Additionally, need to set folder permissions:<br>
  ```bash
  chmod 777 <host folder>
  ```


### 👌 Preparation: Set VM share folder
+ In the details of the Virtual Machine: <br>
  - Add Hardware > Filesystem > Set:
    - Driver: Path
    - Mode: Mapped
    - Source Path: /home/$USER/Public
    - Target Path: /share
    - Optionally and for safety check: export the filesystem as readonly mount


### 👉 Mounting: Manual
```bash
sudo mount -t 9p -o trans=virtio /share \<folder\_path\>
```


### 👉 Mounting: Automatic with Fstab
open the fstab file and add: <br>
/share	\<folder\_path\>	9p	trans=virtio,version=9p2000.L,rw	0	0


<br>
<br>


## 📋 Share Clipboard
TODO: write this
