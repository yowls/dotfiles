#!/usr/bin/env bash

##################################################
#  ·▄▄▄▄        ▄▄▄▄▄·▄▄▄▪  ▄▄▌  ▄▄▄ ..▄▄ ·      #
#  ██▪ ██ ▪     •██  ▐▄▄·██ ██•  ▀▄.▀·▐█ ▀.      #
#  ▐█· ▐█▌ ▄█▀▄  ▐█.▪██▪ ▐█·██▪  ▐▀▀▪▄▄▀▀▀█▄     #
#  ██. ██ ▐█▌.▐▌ ▐█▌·██▌.▐█▌▐█▌▐▌▐█▄▄▌▐█▄▪▐█     #
#  ▀▀▀▀▀•  ▀█▄▀▪ ▀▀▀ ▀▀▀ ▀▀▀.▀▀▀  ▀▀▀  ▀▀▀▀      #
#   gitlab.com/yowls/dotfiles                    #
#                                                #
# Shell script to manage dotfiles                #
# using gnu stow                                 #
# This file must be in ~/.dotfiles dir           #
##################################################

# IMPORT USER SETTINGS
source settings

# VARIABLES
CONFIGS_DIR="config"
CONFIGS_PATH="${HOME}/.dotfiles/${CONFIGS_DIR}"
TARGET_DIR="${HOME}/"

# DEFINE COLORS
colorRed="\033[0;31m"
colorYellow="\033[0;33m"
colorGreen="\033[0;32m"
colorBlue="\033[0;34m"
colorGray="\033[0;37m"
colorEnd="\033[0m"


#################################################

# TODO: pending integration:
# tilda
# conky
# mpv
# ncmpcpp
# qutebrowser
# zathura
# emacs
# polybar, tint2
# openbox, ..
# stump, ..

#################################################

# CHECK IF STOW IS INSTALLED
# FIXME: fix sudo
if [ ! $(command -v stow) ]; then
	echo -e "$colorRed[ERROR]$colorEnd Install stow first"
	read -p "Install now?: (y/N)" USER_INPUT

	if [ $USER_INPUT = "y" ] || [ $USER_INPUT = "Y" ]; then
		if [ $(command -v pacman) ]; then pm="pacman -Sy"
		elif [ $(command -v apt) ]; then pm="apt install"
		elif [ $(command -v xbps-install) ]; then pm="xbps-install -S"
		elif [ $(command -v eopkg) ]; then pm="eopkg install"
		elif [ $(command -v dnf) ]; then pm="dnf in"
		elif [ $(command -v zypper) ]; then pm="zypper in"
		else
			echo -e "No package manager found\nExiting.."
			exit 1
		fi

		sudo $pm " stow"

	else
		exit 1
	fi
fi

#################################################

[ $PROFILE = "auto" ] && PROFILE="$(uname -n)"

welcome(){
	cat << EOF
##################################################
#  ·▄▄▄▄        ▄▄▄▄▄·▄▄▄▪  ▄▄▌  ▄▄▄ ..▄▄ ·      #
#  ██▪ ██ ▪     •██  ▐▄▄·██ ██•  ▀▄.▀·▐█ ▀.      #
#  ▐█· ▐█▌ ▄█▀▄  ▐█.▪██▪ ▐█·██▪  ▐▀▀▪▄▄▀▀▀█▄     #
#  ██. ██ ▐█▌.▐▌ ▐█▌·██▌.▐█▌▐█▌▐▌▐█▄▄▌▐█▄▪▐█     #
#  ▀▀▀▀▀•  ▀█▄▀▪ ▀▀▀ ▀▀▀ ▀▀▀.▀▀▀  ▀▀▀  ▀▀▀▀      #
##################################################

EOF
}

# TODO: show option to install for root
show_help() {
	cat << EOF
Manage dotfiles with stow
Usage: $0 [install | clean | apply | sync | fix | help]

	install         | Install some necessary things and apply the symlinks
	clean           | Remove all symlinks
	apply           | Apply symlinks without clean previous
	sync            | Remove all symlinks and reapply them
	fix             | Remove broken symlinks
	help            | Show this message

EOF
}

fix_broken() {
	if [ $(command -v find) ]; then
		find ~ -maxdepth 3 -xtype l -delete
	else
		echo -e "$colorRed[ALERT]$colorEnd 'find' not installed"
	fi
}

clean() {
	# Remove created symlink with ln -s
	CREATED_SYMLINKS=(
		"$HOME/.config/shell_alias/profile"
		"$HOME/.config/fish/profile.fish"
		"$HOME/.config/zsh/profile.zsh"
		"$HOME/.config/kitty/profile.conf"
		"$HOME/.Xresources.d/profile"
	)
	for symlink in ${CREATED_SYMLINKS[@]}; do
		[ -f $symlink ] && rm $symlink
	done

	# Clean regular apps
	for app in $CONFIGS_DIR/*; do
		stow --dotfiles --verbose --dir=$CONFIGS_DIR --target=$TARGET_DIR --delete "${app##*/}/"
	done

	# Remove created directories
	CREATED_DIRS=(
		"$HOME/.config/fish"
		"$HOME/.config/nvim"
		"$HOME/.config/mpd"
		"$HOME/.config/gtk-3.0"
	)
	for dir in ${CREATED_DIRS[@]}; do
		rmdir --ignore-fail-on-non-empty $dir
	done

	echo -e "============= CLEANED ============= \n"
}

apply() {
	# NEOVIM SETUP
	if [ $NVIM_LANG = "lua" ]; then
		stow --dotfiles --verbose --dir=$CONFIGS_DIR --target=$TARGET_DIR --stow "neovim-lua"
	elif [ $NVIM_LANG = "vim" ]; then
		stow --dotfiles --verbose --dir=$CONFIGS_DIR --target=$TARGET_DIR --stow "neovim-vim"
	fi

	# SHELL SETUP
	if [ ! -z $SHELL_LANG ]; then
		# LINK ALIASES
		stow --dotfiles --verbose --dir=$CONFIGS_DIR --target=$TARGET_DIR --stow "shell_alias"
		SHELL_PROFILE_ALIAS="$CONFIGS_PATH/shell_alias/.config/shell_alias/profiles/$PROFILE"
		if [ -f $SHELL_PROFILE_ALIAS ]; then
			ln -s $SHELL_PROFILE_ALIAS $HOME/.config/shell_alias/profile
			echo "LINK: $SHELL_PROFILE_ALIAS => $HOME/.config/shell_alias/profile"
		fi

		# LINK SHELL CONFIG
		for sh in ${SHELL_LANG[@]}; do
			stow --dotfiles --verbose --dir=$CONFIGS_DIR --target=$TARGET_DIR --stow "$sh"

			# TODO: fix to match bash config
			SHELL_PROFILE_CONFIG="$CONFIGS_PATH/$sh/.config/$sh/profiles/$PROFILE.$sh"
			if [ -f $SHELL_PROFILE_CONFIG ]; then
				ln -s $SHELL_PROFILE_CONFIG $HOME/.config/$sh/profile.$sh
				echo "LINK: $SHELL_PROFILE_CONFIG => $HOME/.config/$sh/profile.$sh"
			fi
		done
	fi

	# REST OF APPLICATIONS
	if [ ! -z $LIST_APPS ]; then
		for i in ${LIST_APPS[@]}; do
			if [ $i = "fish" ] || [ $i = "nvim" ] || [ $i = "mpd" ] || [ $i = "gtk-3.0" ]; then
				# Create this folders before apply link
				# So auto-generated files are not in the repository
				[ ! -d $HOME/.config/$i ] && mkdir $HOME/.config/$i
			fi

			stow --dotfiles --verbose --dir=$CONFIGS_DIR --target=$TARGET_DIR --stow "$i"

			KITTY_PROFILE="$CONFIGS_PATH/kitty/.config/kitty/profiles/$PROFILE.conf"
			if [ $i = "kitty" ] && [ -f $KITTY_PROFILE ]; then
				ln -s $KITTY_PROFILE $HOME/.config/kitty/profile.conf
				echo "LINK: $KITTY_PROFILE => $HOME/.config/kitty/profile.conf"
			fi

			XRESOURCES_PROFILE="$CONFIGS_PATH/Xresources/dot-Xresources.d/profiles/$PROFILE"
			if [ $i = "Xresources" ] && [ -f $XRESOURCES_PROFILE ]; then
				ln -s $XRESOURCES_PROFILE $HOME/.Xresources.d/profile
				echo "LINK: $XRESOURCES_PROFILE => $HOME/.Xresources.d/profile"
			fi
		done
	fi

	echo -e "============= READY ============= \n"
}

#################################################

OPTION=${1:-none}

# TODO: ask to install root config
# TOOD: if lua else if vim
# TODO: reference additional deps for neovim-vim (node, yarn, ..)
# TODO: fix INITIAL_FOLDERS
if [ $OPTION = "install" ]; then
	echo -e "$colorYellow[Alert]$colorEnd Installing.."

	welcome

	# NEOVIM SETUP
	if [ $NVIM_LANG = "lua" ]; then
		PACKER_DIR="$HOME/.local/share/nvim/site/pack/packer/start/packer.nvim"

		if [ ! -d $PACKER_DIR ]; then
			# TODO: check if git is installed
			git clone --depth 1 https://github.com/wbthomason/packer.nvim \
				~/.local/share/nvim/site/pack/packer/start/packer.nvim
		fi

	else
		PLUG_FILE="${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim

		if [ ! -f $PLUG_FILE ]; then
			sh -c 'curl -fLo "$PLUG_FILE --create-dirs \
				https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
		fi
	fi

	for dir in ${INITIAL_FOLDERS[@]}; do
		[ ! -d $dir ] && mkdir $dir
	done

	echo -e "$colorYellow[Alert]$colorEnd Applying symlinks.."
	apply

	# TODO: DO IT AUTOMATICALLY
	echo -e "$colorYellow[Alert]$colorEnd Now finish the installation creating symlinks for some apps:"
	echo -e "   -> kitty (symlinks for the theme)"
	echo -e "   -> autostart file"

elif [ $OPTION = "clean" ]; then
	echo -e "$colorYellow[Alert]$colorEnd Removing symlinks.."
	clean

	for dir in ${INITIAL_FOLDERS[@]}; do
		[ -d $dir ] && rmdir --ignore-fail-on-non-empty $dir
	done

	fix_broken

elif [ $OPTION = "apply" ]; then
	echo -e "$colorYellow[Alert]$colorEnd Applying symlinks without clean previous.."
	apply

elif [ $OPTION = "sync" ]; then
	echo -e "$colorYellow[Alert]$colorEnd Updating symlinks.."
	clean
	apply

elif [ $OPTION = "fix" ]; then
	echo -e "$colorYellow[Alert]$colorEnd Fixing broken symlinks.."
	fix_broken

elif [ $OPTION = "help" ] || [ $OPTION = "-h" ] || [ $OPTION = "--help" ]; then
	show_help

else
	echo -e "$colorRed[Alert]$colorEnd Parameter expected"
	show_help
fi
